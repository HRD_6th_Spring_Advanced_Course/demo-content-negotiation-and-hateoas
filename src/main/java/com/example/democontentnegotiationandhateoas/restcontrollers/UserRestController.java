package com.example.democontentnegotiationandhateoas.restcontrollers;


import com.example.democontentnegotiationandhateoas.models.Comment;
import com.example.democontentnegotiationandhateoas.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/users")
@ExposesResourceFor(User.class)
public class UserRestController {

    @Autowired
    private EntityLinks entityLinks;

    List<User> userList = new ArrayList<>();

    {
        userList.add(new User(1, "Ly Na", "female", Arrays.asList(new Comment(1, "Good News"), new Comment(2, "Bad News"))));
        userList.add(new User(2, "Thorn", "male", Arrays.asList(new Comment(3, "Beautiful girl"),new Comment(3, "Beautiful girl"))));
        userList.add(new User(3, "Virak", "male"));
        userList.add(new User(4, "Pheaktra", "male"));
    }

    @GetMapping("")
    public ResponseEntity<Resources<User>> getAll() {

        Link link = ControllerLinkBuilder.linkTo(UserRestController.class).withSelfRel();

        Resources<User> resources = new Resources<>(userList, link);

        for (User u :
                userList) {
            Integer userId = u.getUid();

            Link userLink = this.entityLinks.linkToSingleResource(User.class, userId).withSelfRel();
            u.removeLinks();

            u.add(userLink);

            Link linkToAllUserComments =
                    ControllerLinkBuilder.
                            linkTo(ControllerLinkBuilder.methodOn(UserRestController.class).
                                    getCommentByUserId(userId)).withRel("comments");

            u.add(linkToAllUserComments);

            if (u.getComments() != null) {
                for (Comment c :
                        u.getComments()) {

                    Integer cId = c.getCid();
                    Link commentLink = ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(CommentRestController.class).getOne(cId)).withSelfRel();
                    c.add(commentLink);
                }
            }

        }

        return ResponseEntity.ok(resources);
    }


    @GetMapping("/{id}")
    public User getOne(@PathVariable("id") Integer id) {

//        Link link = ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(UserRestController.class).getOne(id)).withSelfRel();
//        Link link = ControllerLinkBuilder.linkTo(UserRestController.class).slash(id).withSelfRel();
        Link link = this.entityLinks.linkFor(User.class).slash(id).withSelfRel();

        for (User u :
                userList) {
            if (u.getUid().equals(id)) {

                return u;
            }
        }
        return null;
    }


    @GetMapping("/{user_id}/comments")
    public List<Comment> getCommentByUserId(@PathVariable("user_id") Integer id){
        Link link = ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(UserRestController.class).getCommentByUserId(id)).withSelfRel();

        System.out.println(userList.size());

        for (int i = 0; i < userList.size(); i++) {

            List<Comment> comments = new ArrayList<>();
            if (userList.get(i).getComments() != null) {
                if (userList.get(i).getUid().equals(id)) {
                    comments.addAll(userList.get(i).getComments());
                    return comments;
                }
            }
        }
        return null;
    }
}
