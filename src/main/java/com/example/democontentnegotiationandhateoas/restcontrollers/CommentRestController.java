package com.example.democontentnegotiationandhateoas.restcontrollers;

import com.example.democontentnegotiationandhateoas.models.Comment;
import com.example.democontentnegotiationandhateoas.models.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/comments")
public class CommentRestController {

    List<Comment> comments = new ArrayList<>();
    {
        comments.add(new Comment(1, "Good News"));
        comments.add(new Comment(2, "Bad News"));
        comments.add(new Comment(3, "Beautiful girl"));
    }

    @GetMapping("/{id}")
    public Comment getOne(@PathVariable Integer id){
        for (Comment c :
                comments) {

            if (id.equals(c.getCid()))
                return c;
        }
        return null;
    }









}
