package com.example.democontentnegotiationandhateoas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoContentNegotiationAndHateoasApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoContentNegotiationAndHateoasApplication.class, args);
    }
}
