package com.example.democontentnegotiationandhateoas.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.hateoas.ResourceSupport;

import java.util.List;

public class User extends ResourceSupport {

    private Integer uid;
    private String name;
    private String gender;

    @JsonIgnore
    private List<Comment> comments;

    public User() {
    }

    public User(Integer uid, String name, String gender) {
        this.uid = uid;
        this.name = name;
        this.gender = gender;
    }

    public User(Integer uid, String name, String gender, List<Comment> comments) {
        this.uid = uid;
        this.name = name;
        this.gender = gender;
        this.comments = comments;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    @Override
    public String toString() {
        return "User{" +
                "uid=" + uid +
                ", name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", comments=" + comments +
                '}';
    }
}
