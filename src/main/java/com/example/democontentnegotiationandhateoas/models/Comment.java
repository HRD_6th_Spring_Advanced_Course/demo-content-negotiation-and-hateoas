package com.example.democontentnegotiationandhateoas.models;

import org.springframework.hateoas.ResourceSupport;

public class Comment extends ResourceSupport {

    private Integer cid;
    private String desc;

    private User user;

    public Comment() {
    }

    public Comment(Integer cid, String desc) {
        this.cid = cid;
        this.desc = desc;
    }

    public Comment(Integer cid, String desc, User user) {
        this.cid = cid;
        this.desc = desc;
        this.user = user;
    }

    public Integer getCid() {
        return cid;
    }

    public void setCid(Integer cid) {
        this.cid = cid;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "cid=" + cid +
                ", desc='" + desc + '\'' +
                ", user=" + user +
                '}';
    }
}
